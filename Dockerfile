# Use the LTS version of node as the base image
FROM node:16-alpine3.14

WORKDIR /usr/app

# Copy package.json and install the required node modules
COPY ./app/package.json .
RUN npm install