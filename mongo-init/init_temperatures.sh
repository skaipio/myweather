#!/bin/bash

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
csv_path="$script_dir/helsinki_temperatures.csv"
echo "Reading temperatures from $csv_path"

documents=""

# Get the environment variables from the user shell
set -e

# Read the temperatures from the CSV file
while IFS="," read -r col_year col_month col_day col_time col_timezone col_temperature
do
# Set leading zeros to month and day 
month=$(printf '%02d\n' $col_month)
day=$(printf '%02d\n' $col_day)
# Form the full timestamp from the parts
timestamp="$col_year-$month-${day}T${col_time}Z"
document="{timestamp: '$timestamp', temperature: $col_temperature, location: 'Helsinki'}"

echo "Inserting a document"
echo "$document"

# Insert the measurements into the database
mongo <<EOF
use $MONGO_INITDB_DATABASE

db.measurements.insertOne($document)
EOF

#documents+=",$document"
done < <(tail -n +2 $csv_path)

# Drop the first colon and wrap with brackets
#documents="[${documents:1}]"




