set -e

mongo <<EOF
use $MONGO_INITDB_DATABASE

db.createUser({
  user: "$MONGO_MYWEATHER_USER",
  pwd: "$MONGO_MYWEATHER_USER_PASSWORD",
  roles: [
    { role: 'readWrite', db: "$MONGO_INITDB_DATABASE"}
  ]
})

db.users.createIndex(
  {
      "username": 1
  },
  {
      unique: true
  }
)

db.users.insertOne({
  username: "$MYWEATHER_DEMO_USER",
  passwordHash: "$MYWEATHER_DEMO_USER_PASSWORD_HASH",
  passwordSalt: "$MYWEATHER_DEMO_USER_PASSWORD_SALT"
})
EOF