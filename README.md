# MyWeather

This is a demo project for displaying temperatures recorded at Helsinki, Kumpula. The records are static and are initialized from `./mongo-init/helsinki_temperatures.csv`.

## Requirements
Docker must be installed on the system to be able to run MongoDB.

Node.js is optional. The project can be developed without installing Node.js by running the development server in Docker. The source files are mounted to the container. The project has been developed using Node.js 16.14.2.

## Getting Started
Before running the server, you should setup the required [environment variables](#environment). For a quick start you can copy `.env.example` and rename it `.env`.

### Running on Docker
 Start the application in development mode by running
```sh
docker compose up -d -V
```

Start the application in production mode by running
```sh
docker compose -f docker-compose.production.yml up -d -V
```

If changes are made to `package.json`, then the above commands should be run with the build flag to update the Docker images.
```sh
docker compose up -d -V --build
```

### Running on Node
First start MongoDB by running
```sh
docker compose -f docker-compose.mongo.yml up -d
```

Install all the required node modules.
```sh
npm install
```

Start the application in development mode by running
```sh
sh -c "cd app && npm run dev"
```

Start the application in production mode by running
```sh
sh -c "cd app && npm run build && npm run dev"
```

### Open the application
Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

The **default credentials** for a demo user are
```sh
# username
myweatheruser
# password
awesomellama
```
To force logout, one can delete the `myweather` cookie from the browser.

Hot-reload is on in development mode meaning that pages are auto-updated as they are edited.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

### Environment
To override any environment variables, create a `.env` file. See `.env.example` for a detailed description of required variables. The following are the default environment variables:
```sh
MONGO_INITDB_DATABASE=myweather
MONGO_INITDB_ROOT_USERNAME=root
MONGO_INITDB_ROOT_PASSWORD=happyllama
MONGO_MYWEATHER_USER=myweather
MONGO_MYWEATHER_USER_PASSWORD=grumpyllama
MYWEATHER_DEMO_USER=myweatheruser
MYWEATHER_DEMO_USER_PASSWORD_HASH='e49f94b04db13382abbdd5054d0a431591080e86ce185df0d144dc8f63538b0ade2c730256d9f84e0311c4470ddce7506a70bdac3272ff618d7d5ef2852fb4f9'
MYWEATHER_DEMO_USER_PASSWORD_SALT='c2acd62406e9fba8c1f220f3abd44e3b'
MONGODB_URI='mongodb://myweather:grumpyllama@mongo:27017/myweather'
AUTH_COOKIE_PASSWORD='bGtqw7ZsZXJnasO2bHJnamHDtnJnamlh'
ME_CONFIG_MONGODB_ADMINUSERNAME=root
ME_CONFIG_MONGODB_ADMINPASSWORD=happyllama
ME_CONFIG_MONGODB_URL='mongodb://root:happyllama@mongo:27017/'
```

Note that everytime any changes are made to the environment variables, the app server should be restarted for them to come into effect.

### Running tests
Running tests requires Node.js. They can be run by running
```sh
npm test
```

## API
The API documentation is served as Swagger documentation. When running the server, the API documentation is available at [http://localhost:3000/api-doc](http://localhost:3000/api-doc).

## User Flow
A simple flow chart for the user journey.

<img src="./user-flow.svg" />

## Next.js
This project has been built on Next.js. To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
