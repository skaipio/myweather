import IMeasurement from "../types/measurement"

export const measurementsApiPath = '/api/measurements'

type ApiResponse<Type> = {
  data?: Type
  message?: string
  success: boolean
}

type MeasurementApiResponse = ApiResponse<IMeasurement>

const fetchJson = async (
  path: string,
  measurement: IMeasurement,
  method: string,
  headers?: HeadersInit
): Promise<MeasurementApiResponse> => {
  const contentType = 'application/json'
  const req = {
    method,
    headers: {
      Accept: contentType,
      'Content-Type': contentType,
      ...headers,
    },
    body: method.toLowerCase() === 'delete' ? null : JSON.stringify(measurement),
  }

  const res = await fetch(path, req)
  return await res.json() as MeasurementApiResponse
}

const getIdPath = (measurement: IMeasurement) => {
  if (!measurement.id) {
    throw new Error('No id on measurement to append to API request')
  }
  return `${measurementsApiPath}/${measurement.id}`
}


export async function deleteMeasurement(measurement: IMeasurement) {
  try {
    return await fetchJson(getIdPath(measurement), measurement, 'DELETE')
  } catch (error) {
    console.log(`Failed to delete measurement ${measurement.id}`)
  }
}

export async function getMeasurements() {
  const res = await fetch(measurementsApiPath)
  const { data } = await res.json()

  return (data || []) as IMeasurement[]
}

export async function postMeasurement(measurement: IMeasurement): Promise<MeasurementApiResponse> {
  const contentType = 'application/json'
  const res = await fetch(measurementsApiPath, {
    method: 'POST',
    headers: {
      Accept: contentType,
      'Content-Type': contentType,
    },
    body: JSON.stringify(measurement),
  })

  return await res.json()
}

export async function putMeasurement(measurement: IMeasurement): Promise<MeasurementApiResponse> {
  return await fetchJson(getIdPath(measurement), measurement, 'PUT')
}

/** Adds a field 'fahrenheit' with temperature converted to fahrenheit degrees */
export const enrichMeasurements = (measurements: IMeasurement[]) => {
  return measurements.map(measurement => {
    const fahrenheit = measurement.temperature * 1.8 + 32
    return {
      ...measurement,
      fahrenheit
    }
  })
}