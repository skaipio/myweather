import type { NextApiResponse } from 'next'


export async function sendMessageOnError(handler: () => Promise<any>, res: NextApiResponse) {
  try {
    await handler()
  } catch (error) {
    let errorJson: any = {
      success: false
    }
    if (error instanceof Error) {
      errorJson.message = error.message
    }
    console.error(error)
    res.status(400).json(errorJson)
  }
}