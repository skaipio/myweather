import { AlertColor } from "@mui/material"
import { createContext } from "react"

export type AlertMessage = { message: string, severity: AlertColor }

export type AppContextType = {
  alertMessage?: AlertMessage | null
  setAlertMessage: (alertMessage: AlertMessage) => void
}

const defaultContext: AppContextType = {
  setAlertMessage: (alertMessage: AlertMessage) => { }
}

const AppContext = createContext(defaultContext)

export default AppContext