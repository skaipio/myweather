import { act, render, screen } from '@testing-library/react'
import Home from '../pages/index'

const loggedInUser = {
  isLoggedIn: true
}

const loggedOutUser = {
  isLoggedIn: false
}

let mockUser = loggedOutUser

jest.mock('../lib/useUser', () => {
  const originalModule = jest.requireActual('../lib/useUser');

  //Mock the default export and named export 'foo'
  return {
    __esModule: true,
    ...originalModule,
    default: jest.fn((b) => {
      return { user: mockUser }
    }),
  };
});

jest.mock('../lib/useUser')

describe('Home', () => {
  let container

  beforeEach(async () => {
    // setup a DOM element as a render target
    container = document.createElement('div')
    document.body.appendChild(container)
  })

  it('renders an empty page when the user is not logged in', async () => {
    mockUser = loggedOutUser

    // Wait for the data grid to be fully rendered
    await act(() => render(
      <Home />
    ))

    const title = screen.queryByText('MyWeather')
    expect(title).toBeFalsy()
    const table = screen.queryByRole('table')
    expect(table).toBeFalsy()
  })

  it('renders the site title when the user is logged in', async () => {
    mockUser = loggedInUser

    // Wait for the data grid to be fully rendered
    await act(() => render(
      <Home />
    ))

    const title = screen.queryByText('MyWeather')
    expect(title).toBeInTheDocument()
  })

  it('renders the measurement table when the user is logged in', async () => {
    mockUser = loggedInUser

    // Wait for the data grid to be fully rendered
    await act(() => render(
      <Home />
    ))

    const table = screen.queryByRole('table')
    expect(table).toBeInTheDocument()
  })

  it('renders a logout button when the user is logged in', async () => {
    mockUser = loggedInUser

    // Wait for the data grid to be fully rendered
    await act(() => render(
      <Home />
    ))

    const logoutButton = screen.queryByText (/logout/i)
    expect(logoutButton).toBeInTheDocument()
  })
})