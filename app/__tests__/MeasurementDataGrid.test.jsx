import { act, queryByText, render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import FetchMock from 'jest-fetch-mock'
import MeasurementDataGrid from '../components/MeasurementDataGrid'

const mockMeasurements = [
  {
    id: '1',
    temperature: 16.7,
    timestamp: '2022-04-08T13:00:00Z'
  },
  {
    id: '2',
    temperature: 17.4,
    timestamp: '2022-04-08T14:00:00Z'
  },
  {
    id: '3',
    temperature: 18.5,
    timestamp: '2022-04-08T15:00:00Z'
  },
  {
    id: '4',
    temperature: 20.0,
    timestamp: '2022-04-08T16:00:00Z'
  },
]

jest.mock('swr', () => {
  const originalModule = jest.requireActual('swr');

  //Mock the default export
  return {
    __esModule: true,
    ...originalModule,
    default: jest.fn(() => {
      return { data: mockMeasurements, mutate: () => { } }
    }),
  };
});

const queryNewMeasurementButton = () => screen.findByText('New measurement')
const querySwitchTemperatureButton = () => screen.queryByText(/To °(C|F)/i)
const addMeasurementLabel = 'add a measurement'
const confirmEditLabel = 'confirm edit'

// See https://www.npmjs.com/package/jest-fetch-mock
// on how to mock with FetchMock
FetchMock.enableMocks()

describe('MeasurementDataGrid', () => {
  let container

  beforeEach(async () => {
    // setup a DOM element as a render target
    container = document.createElement('div')
    document.body.appendChild(container)

    //jest.useFakeTimers()
    fetch.resetMocks()
    FetchMock.mockResponseOnce(JSON.stringify(mockMeasurements))

    // Wait for the data grid to be fully rendered
    waitFor(() => render(
      <MeasurementDataGrid />
    ))
  })

  afterEach(() => {
    // cleanup on exiting
    container.remove()
    container = null
    //jest.useRealTimers()
  })

  it('renders a toolbar with buttons for adding a new measurement and switching the temperature unit', () => {
    expect(queryNewMeasurementButton()).toBeTruthy()
    expect(querySwitchTemperatureButton()).toBeTruthy()
  })

  it('renders column headers to the data grid', async () => {
    expect(screen.queryByText('Date')).toBeTruthy()
    expect(screen.queryByText(/temperature/i)).toBeTruthy()
  })

  it('renders given measurements along with timestamps to the data grid', async () => {
    for (const measurement of mockMeasurements) {
      // Use find by when checking for elements that are loaded asynchronously
      // Need to format the timestamp before checking
      //expect(screen.findByText(measurement.timestamp)).toBeTruthy()
      expect(screen.findByText(measurement.temperature)).toBeTruthy()
    }
  })

  xit('renders a new editable measurement at the top of the data grid when clicking on "New measurement" button', async () => {
    await waitFor(() => userEvent.click(queryNewMeasurementButton()))
  })

  xit('editable measurement is disabled until all required inputs are filled', async () => {
    await waitFor(() => userEvent.click(queryNewMeasurementButton()))

    // TODO: Check inputs

    await waitFor(() => userEvent.click(screen.findByLabelText(confirmEditLabel)))
  })

  xit('editable measurement is closed when the "confirm edit" button is clicked', async () => {
    await act(() => userEvent.click(screen.queryByLabelText(addMeasurementLabel)))
    await act(() => userEvent.click(screen.queryByLabelText(confirmEditLabel)))

    expect(screen.queryByLabelText(dateColumnHeader)).toBeFalsy()
    expect(screen.queryByLabelText(temperatureColumnHeader)).toBeFalsy()
  })

  xit('editable measurement is closed when the "cancel edit" button is clicked', async () => {
    await act(() => userEvent.click(screen.queryByLabelText(addMeasurementLabel)))
    await act(() => userEvent.click(screen.queryByLabelText('cancel edit')))

    expect(screen.queryByLabelText(dateColumnHeader)).toBeFalsy()
    expect(screen.queryByLabelText(temperatureColumnHeader)).toBeFalsy()
  })
})