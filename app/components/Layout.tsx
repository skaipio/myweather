import { Alert, Box, Container, Snackbar } from '@mui/material'
import Head from 'next/head'
import { useState } from 'react'
import AppContext, { AlertMessage } from '../lib/appContext'
import styles from '../styles/Home.module.css'
import Header from './Header'

export default function Layout({ children }: { children: React.ReactNode }) {
  const [alertMessage, setAlertMessage] = useState<AlertMessage | null>(null)

  const closeSnackbar = () => {
    setAlertMessage(null)
  }

  return (
    <>
      <Head>
        <title>MyWeather</title>
        <meta name="description" content="Weather measurements" />
        <meta name="viewport" content="initial-scale=1, width=device-width" />
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
      </Head>
      <AppContext.Provider value={{
        setAlertMessage
      }}>
        <Header />
        <Snackbar
          id='appSnackBar'
          open={!!alertMessage}
          autoHideDuration={6000}
          onClose={closeSnackbar}
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
          <Alert onClose={closeSnackbar} severity={alertMessage?.severity} sx={{ width: '100%' }}>
            {alertMessage?.message}
          </Alert>
        </Snackbar>
        <main>
          <Container>
            {children}
          </Container>
        </main>
      </AppContext.Provider>
      <footer className={styles.footer}>
        <Box sx={{display: 'inline-flex'}}>
          <span>MyWeather at&nbsp;</span><a href='https://gitlab.com/skaipio/myweather'>GitLab</a>
        </Box>
      </footer>
    </>
  )
}