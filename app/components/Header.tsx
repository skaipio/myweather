import { AppBar, Button, Toolbar, Typography } from "@mui/material"
import { useRouter } from "next/router"
import { MouseEvent } from "react"
import useUser from "../lib/useUser"
import IUser from "../types/user"

const logoutFetcher = async () => {
  const res = await fetch('/api/logout', { method: 'POST' })
  const { data } = await res.json()
  return data as IUser
}

export default function Header() {
  const { user, mutateUser } = useUser()
  const router = useRouter()

  const handleLogoutClick = async (e: MouseEvent) => {
    e.preventDefault()
    mutateUser(
      await logoutFetcher(),
      false
    )
    router.push('/login')
  }

  return (
    <AppBar position="static">
      <Toolbar sx={{ justifyContent: 'space-between' }}>
        <Typography variant="h6" color="inherit" component="div">
          MyWeather
        </Typography>
        {user && user.isLoggedIn &&
          <Button disableElevation href="/api/logout" onClick={handleLogoutClick} variant="contained">
            Logout
          </Button>
        }
      </Toolbar>
    </AppBar>
  )
}