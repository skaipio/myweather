import CancelOutlinedIcon from '@mui/icons-material/CancelOutlined';
import CheckIcon from '@mui/icons-material/Check';
import DeleteIcon from '@mui/icons-material/Delete';
import { Box, IconButton, TableCell, TableRow, TextField, TextFieldProps, Tooltip } from '@mui/material';
import { DateTime } from 'luxon';
import { ChangeEvent, MouseEvent, RefObject, useEffect, useRef, useState } from 'react';
import IMeasurement from '../../types/measurement';
import styles from './MeasurementDataGrid.module.css';


const MeasurementTextField = (props: TextFieldProps) => {
  return (
    <TextField
      InputLabelProps={{
        shrink: true,
      }}
      required
      {...props} />
  )
}

let dateFormatter: Intl.DateTimeFormat

type MeasurementRowProps = {
  defaultMeasurement: IMeasurement
  isSelected: boolean,
  onClickCancel: (measurement: IMeasurement) => void
  onClickDelete?: (measurement: IMeasurement) => void
  onClickMeasurement?: (measurement: IMeasurement) => void
  onRefReceived?: (ref: RefObject<HTMLTableRowElement>) => void
  onUpdateMeasurement: (measurement: IMeasurement) => void
  temperatureUnit: string
}

/**
 * Renders a row on the measurements table. When selected, displays the measurement
 * as editable text fields. Has buttons for confirming or canceling an update to the measurement
 * and for deleting the measurement.
 * TODO: Selection is slow at the moment because the table has to be rerendered on selection.
 * Move to Context.
 */
function MeasurementRow({
  defaultMeasurement,
  isSelected,
  onClickCancel,
  onClickDelete,
  onClickMeasurement,
  onRefReceived,
  onUpdateMeasurement,
  temperatureUnit
}: MeasurementRowProps) {
  const rowRef = useRef<HTMLTableRowElement>(null)
  // window.navigator is not defined in the outer context so we have to
  // initialize it here and cache it
  dateFormatter = dateFormatter || new Intl.DateTimeFormat(
    window.navigator.language,
    { year: "numeric", month: "2-digit", day: "2-digit", hour: "2-digit", minute: "2-digit" }
  )

  useEffect(() => {
    onRefReceived && onRefReceived(rowRef)
  }, [rowRef])

  // Convert the measurement timestamp from UTC to local time and drop the offset
  // for it to work with the datetime selector
  const convertTimestamp = (timestamp: string) => {
    const dt = DateTime.fromISO(timestamp)
    return dt.toLocal().set({ second: 0, millisecond: 0 }).toISO({
      includeOffset: false,
      suppressSeconds: true,
      suppressMilliseconds: true,
    })
  }

  const isNewMeasurement = !defaultMeasurement.id

  // Need to convert measurement fields to strings for them to properly work in text fields
  const initialEditableMeasurement = {
    ...(defaultMeasurement as any),
    temperature: defaultMeasurement.temperature.toString(),
    timestamp: convertTimestamp(defaultMeasurement.timestamp as string)
  }

  const [editableMeasurement, setEditableMeasurement] = useState(initialEditableMeasurement)
  const [touched, setTouched] = useState(isNewMeasurement)

  // Convert measurement with string fields back to a proper measurement
  const convertEditableMeasurementToMeasurement = () => {
    return {
      ...editableMeasurement,
      // Convert back to float
      temperature: parseFloat(editableMeasurement.temperature),
      // Convert back to a UTC string
      timestamp: DateTime.fromISO(editableMeasurement.timestamp).toUTC().toISO()
    } as IMeasurement
  }

  const handleFieldChange = (
    e: ChangeEvent<HTMLInputElement>
  ) => {
    const { name } = e.target
    let value = e.target.value
    setEditableMeasurement({ ...editableMeasurement, [name]: value });
    setTouched(true)
  };

  const handeOnClickCancel = (e: MouseEvent<HTMLButtonElement>) => {
    // Need to stop propagation so that onClickMeasurement is not triggered
    e.stopPropagation()
    onClickCancel(convertEditableMeasurementToMeasurement())
  }

  const handleClickConfirmUpdate = (e: MouseEvent<HTMLButtonElement>) => {
    // Need to stop propagation so that onClickMeasurement is not triggered
    e.stopPropagation()
    onUpdateMeasurement(convertEditableMeasurementToMeasurement())
  }

  const handleClickDelete = (e: MouseEvent<HTMLButtonElement>) => {
    // Need to stop propagation so that onClickMeasurement is not triggered
    e.stopPropagation()
    onClickDelete && onClickDelete(convertEditableMeasurementToMeasurement())
  }

  const handleClickMeasurement = () => {
    onClickMeasurement && onClickMeasurement(defaultMeasurement)
  }

  // TODO: Move the conversion somewhere where it isn't done on every render
  // Maybe useMemo?
  const dateStr = dateFormatter.format(new Date(initialEditableMeasurement.timestamp as string))
  const temperatureStr = temperatureUnit === '°F' ?
    defaultMeasurement.fahrenheit?.toFixed(1)
    : defaultMeasurement.temperature.toFixed(1)

  // Get background color for rows according to temperature
  const isFrost = defaultMeasurement.temperature <= 0
  const maxTemp = 30
  const temp = Math.min(Math.abs(defaultMeasurement.temperature), maxTemp)
  const percentage = temp / maxTemp
  const hue = isFrost ? 210 : 25
  const saturation = 50 + percentage * 30
  const lightness = 100 - percentage * 30
  const color = `hsl(${hue}deg ${saturation}% ${lightness}% / 1)`

  return (
    <TableRow
      className={[styles.measurementRow, isSelected && styles.isSelected].join(' ')}
      onClick={handleClickMeasurement}
      ref={rowRef}
      sx={{ backgroundColor: color }}>
      <TableCell>
        <Box className={styles.tableCellContent}>
          {isSelected ?
            <MeasurementTextField
              autoFocus={true}
              name='timestamp'
              onChange={handleFieldChange}
              value={editableMeasurement.timestamp}
              type='datetime-local' />
            : dateStr}
        </Box>
      </TableCell>
      <TableCell sx={{ width: '150px' }}>
        <Box className={styles.tableCellContent} sx={{ alignItems: 'baseline' }}>
          {isSelected ?
            <MeasurementTextField
              name='temperature'
              onChange={handleFieldChange}
              value={editableMeasurement.temperature}
              type='number' />
            : temperatureStr}
          {isSelected && <span style={{ paddingLeft: '8px' }}>°C</span>}
        </Box>
      </TableCell>
      <TableCell sx={{ width: '100px', textAlign: 'center' }}>
        <Box className={styles.tableCellContent} sx={{ visibility: (isSelected ? 'visible' : 'hidden') }}>
          <Tooltip title={isNewMeasurement ? "Confirm add" : "Confirm update"}>
            <span>
              <IconButton
                aria-label='confirm edit'
                color='success'
                disabled={!touched}
                onClick={handleClickConfirmUpdate}>
                <CheckIcon />
              </IconButton>
            </span>
          </Tooltip>
          <Tooltip title={isNewMeasurement ? "Cancel add" : "Cancel update"}>
            <IconButton aria-label='cancel edit' color='warning' onClick={handeOnClickCancel}>
              <CancelOutlinedIcon />
            </IconButton>
          </Tooltip>
          {!isNewMeasurement &&
            <Tooltip title="Delete">
              <IconButton aria-label='delete measurement' color='error' onClick={handleClickDelete}>
                <DeleteIcon />
              </IconButton>
            </Tooltip>}
        </Box>
      </TableCell>
    </TableRow>
  )
}

export default MeasurementRow