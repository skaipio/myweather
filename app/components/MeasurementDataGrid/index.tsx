import InfoIcon from '@mui/icons-material/Info';
import { Box, Button, IconButton, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Tooltip, Typography } from '@mui/material';
import { DateTime } from 'luxon';
import { Component, memo, RefObject, useContext } from 'react';
import useSWR, { KeyedMutator } from 'swr';
import AppContext, { AlertMessage } from '../../lib/appContext';
import { deleteMeasurement, enrichMeasurements, getMeasurements, measurementsApiPath, postMeasurement, putMeasurement } from '../../lib/measurements';
import IMeasurement from '../../types/measurement';
import styles from './MeasurementDataGrid.module.css';
import MeasurementRow from './MeasurementRow';


/**
 * Wrap MeasurementDataGrid and pass measurements as props since hooks
 * cannot be used in class components.
 */
const SWRWrappedMeasurementGrid = () => {
  const { setAlertMessage } = useContext(AppContext)
  // Fetch or get measurements from cache using SWR
  // TODO: Handle the error by showing a snackbar with an error message
  const { data: measurements, mutate } = useSWR(
    measurementsApiPath,
    getMeasurements
  );

  return <MeasurementDataGrid
    measurements={measurements && enrichMeasurements(measurements)}
    mutateMeasurements={mutate}
    setAlertMessage={setAlertMessage} />
}

export default SWRWrappedMeasurementGrid

/** Make a memoized version of MeasurementRow to increase table rendering performance. */
const MemoizedRow = memo(MeasurementRow, (prevProps, nextProps) => {
  return JSON.stringify(prevProps) === JSON.stringify(nextProps)
})

type MeasurementDataGridProps = {
  measurements: IMeasurement[] | undefined;
  mutateMeasurements: KeyedMutator<IMeasurement[]>;
  setAlertMessage: (alertMessage: AlertMessage) => void
};
type MeasurementDataGridState = {
  isAddMeasurementVisible: boolean
  measurements: IMeasurement[] | undefined
  selectedMeasurement: IMeasurement | null
  temperatureUnit: string
};

/** A table for displaying measurements */
class MeasurementDataGrid extends Component<MeasurementDataGridProps, MeasurementDataGridState> {
  defaultMeasurement: IMeasurement = {
    location: 'Helsinki',
    timestamp: DateTime.now().toLocal().toISO(),
    temperature: 0
  }

  state: MeasurementDataGridState = {
    isAddMeasurementVisible: false,
    measurements: undefined,
    selectedMeasurement: null,
    temperatureUnit: '°C'
  }

  mutateMeasurements: KeyedMutator<IMeasurement[]>

  constructor(props: MeasurementDataGridProps) {
    super(props)
    this.mutateMeasurements = props.mutateMeasurements
  }

  getOppositeTemperatureUnit = () => {
    return this.state.temperatureUnit === '°C' ? '°F' : '°C'
  }

  handleClickCancelEdit = () => {
    this.setState({
      ...this.state,
      isAddMeasurementVisible: false,
      selectedMeasurement: null
    })
  }

  handleAddMeasurement = async (measurement: IMeasurement) => {
    const { data: newMeasurement, message, success } = await postMeasurement(measurement)
    if (!success || !newMeasurement) {
      this.props.setAlertMessage({ message: (message || ''), severity: 'error' })
      return
    }

    await this.mutateMeasurements([...this.props.measurements as IMeasurement[], newMeasurement])
    this.setState({
      ...this.state,
      isAddMeasurementVisible: false
    })
    this.props.setAlertMessage({ message: 'Measurement added', severity: 'success' })
  }

  handleClickDelete = async (measurement: IMeasurement) => {
    await deleteMeasurement(measurement)
    let oldMeasurements = this.props.measurements || []
    oldMeasurements = oldMeasurements.filter(oldMeasurement => oldMeasurement.id !== measurement.id)
    await this.mutateMeasurements(oldMeasurements)
    this.setState({
      ...this.state,
      selectedMeasurement: null
    })
    this.props.setAlertMessage({ message: 'Measurement deleted', severity: 'success' })
  }

  handleClickMeasurement = (measurement: IMeasurement) => {
    this.setState({
      ...this.state,
      selectedMeasurement: measurement
    })
  }

  handleClickNewMeasurement = () => {
    this.setState({
      ...this.state,
      isAddMeasurementVisible: true,
      selectedMeasurement: null
    })
  }

  // Used for getting the ref to the row meant for creating a new measurement.
  // As soon as the ref is received, the row has been created and can be scrolled
  // into view.
  handleRowRefReceived = (ref: RefObject<HTMLTableRowElement>) => {
    ref.current?.scrollIntoView({
      behavior: 'smooth',
      block: 'end'
    })
  }

  handleUpdateMeasurement = async (measurement: IMeasurement) => {
    const { data: updatedMeasurement, message, success } = await putMeasurement(measurement)
    if (!success || !updatedMeasurement) {
      this.props.setAlertMessage({ message: (message || ''), severity: 'error' })
      return
    }

    let oldMeasurements = this.props.measurements || []
    const idx = oldMeasurements.findIndex((oldMeasurement) => oldMeasurement.id === measurement.id)
    if (idx >= 0) {
      const updatedMeasurements = [
        ...oldMeasurements.slice(0, idx),
        measurement,
        ...oldMeasurements.slice(idx + 1, oldMeasurements.length)]

      await this.mutateMeasurements(updatedMeasurements)
    }
    this.setState({
      ...this.state,
      selectedMeasurement: null
    })
    this.props.setAlertMessage({ message: 'Measurement updated', severity: 'success' })
  }

  // Renderer for the measurement rows of the data grid
  renderMeasurements = () => this.props.measurements?.map((measurement) =>
    <MemoizedRow key={measurement.id}
      defaultMeasurement={measurement}
      isSelected={measurement.id === this.state.selectedMeasurement?.id}
      onClickCancel={this.handleClickCancelEdit}
      onClickDelete={this.handleClickDelete}
      onClickMeasurement={this.handleClickMeasurement}
      onUpdateMeasurement={this.handleUpdateMeasurement}
      temperatureUnit={this.state.temperatureUnit} />
  )

  switchTemperatureUnit = () => {
    let temperatureUnit = this.getOppositeTemperatureUnit()
    this.setState({
      ...this.state,
      temperatureUnit
    })
  }

  render() {
    const { isAddMeasurementVisible, selectedMeasurement } = this.state

    return (
      <>
        <div className={styles.measurementsToolbar}>
          <Typography variant='h6' className={styles.measurementsHeader}>
            Weather at Helsinki
          </Typography>
          <Box sx={{ display: 'inline-flex', gap: '5px' }}>
            <Button aria-label={`convert to ${this.getOppositeTemperatureUnit()}`}
              size="small"
              onClick={this.switchTemperatureUnit}
              variant="outlined">
              To {this.getOppositeTemperatureUnit()}
            </Button>
            <Button aria-label='new measurement'
              disabled={isAddMeasurementVisible}
              size="small"
              onClick={this.handleClickNewMeasurement}
              variant="outlined">
              New measurement
            </Button>
            <Tooltip title="Click on the measurements to edit them." placement='bottom'>
              <IconButton aria-label='info'>
                <InfoIcon />
              </IconButton>
            </Tooltip>
          </Box>
        </div>
        <TableContainer component={Box} className={styles.measurementsContainer}>
          <Table id="measurement-grid" aria-label="measurement table" role="table" stickyHeader>
            <TableHead>
              <TableRow>
                <TableCell role="columnheader" scope="col">Date</TableCell>
                <TableCell role="columnheader" scope="col">Temperature ({this.state.temperatureUnit})</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <>
                {isAddMeasurementVisible && !selectedMeasurement &&
                  <MeasurementRow
                    key={this.defaultMeasurement.id || this.defaultMeasurement.timestamp.toString()}
                    defaultMeasurement={this.defaultMeasurement}
                    isSelected={true}
                    onClickCancel={this.handleClickCancelEdit}
                    onRefReceived={this.handleRowRefReceived}
                    onUpdateMeasurement={this.handleAddMeasurement}
                    temperatureUnit={this.state.temperatureUnit} />
                }
                {this.renderMeasurements()}
              </>
            </TableBody>
          </Table>
        </TableContainer>
      </>
    )
  }
}