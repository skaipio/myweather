import mongoose, { Model } from 'mongoose'
import IUser from '../types/user';

/* When editing the schema, you may have to restart the dev server
 for the changes to come into effect. */

const UserSchema = new mongoose.Schema<IUser>({
  username: {
    type: String,
    required: [true, 'Username is required'],
    unique: true
  },
  passwordHash: {
    type: String,
    required: [true, 'Password hash is required']
  },
  passwordSalt: {
    type: String,
    required: [true, 'Password salt is required']
  }
})

UserSchema.set('toJSON', {
  virtuals: false,
  transform: (doc, ret, options) => {
    delete ret._id
    delete ret.passwordHash
    delete ret.passwordSalt
  }
});

const model = mongoose.models.User || mongoose.model<IUser>('User', UserSchema)

export default model