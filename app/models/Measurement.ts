/**
 * @swagger
 * components:
 *  schemas:
 *    Measurement:
 *      type: object
 *      properties:
 *        id:
 *          type: string
 *        location:
 *          type: string
 *        temperature:
 *          type: number
 *        timestamp:
 *          type: string
 *          format: date-time
 *      required:
 *        - location
 *        - temperature
 *        - timestamp
 */

import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import IMeasurement from '../types/measurement';

const MeasurementSchema = new mongoose.Schema<IMeasurement>({
  location: {
    type: String,
    required: [true, 'Location is required'],
  },
  temperature: {
    type: Number,
    required: [true, 'Temperature is required']
  },
  timestamp: {
    type: Date,
    required: [true, 'Timestamp is required']
  }
})

MeasurementSchema.set('toJSON', {
  virtuals: false,
  transform: (doc, ret, options) => {
    ret.id = ret._id
    delete ret._id
  }
});

MeasurementSchema.index({ timestamp: 1, location: 1}, { unique: true });

MeasurementSchema.plugin(uniqueValidator)

const model = mongoose.models.Measurement || mongoose.model<IMeasurement>('Measurement', MeasurementSchema)

export default model