export default interface IMeasurement {
  id?: string,
  fahrenheit?: number | undefined;
  location: string;
  temperature: number;
  timestamp: string | Date;
}