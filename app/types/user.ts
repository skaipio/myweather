export default interface IUser {
  isLoggedIn?: boolean;
  username: string;
  passwordHash?: string;
  passwordSalt?: string;
}