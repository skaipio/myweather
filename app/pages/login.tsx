import { Button, TextField } from '@mui/material'
import { Box } from '@mui/system'
import React, { FormEvent, useContext } from 'react'
import Layout from '../components/Layout'
import AppContext from '../lib/appContext'
import useUser from '../lib/useUser'
import styles from '../styles/Login.module.css'
import IUser from '../types/user'

/** A basic login form for the user to enter their credentials and gain access to the main view */
const LoginForm = () => {
  const { mutateUser } = useUser({
    redirectTo: '/',
    redirectIfFound: true,
  })
  const { setAlertMessage } = useContext(AppContext)

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    // Prevent default so that we are not redirected to another page
    event.preventDefault()
    const { username, password } = event.currentTarget

    const body = {
      username: username.value,
      password: password.value,
    }

    const res = await fetch('/api/login', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body),
    })

    const { data, message, success } = await res.json()

    if (res.ok && success) {
      const user = {
        ...data,
        isLoggedIn: true
      } as IUser

      mutateUser(user)
    } else {
      setAlertMessage({ message, severity: 'warning' })
    }
  }

  return (
    <Box
      component='form'
      noValidate sx={{ mt: 1 }}
      onSubmit={handleSubmit}>
      <TextField
        margin="normal"
        required
        fullWidth
        id="username"
        label="User name"
        name="username"
        autoComplete="username"
        autoFocus
      />
      <TextField
        margin="normal"
        required
        fullWidth
        name="password"
        label="Password"
        type="password"
        id="password"
        autoComplete="current-password"
      />
      <Button
        type="submit"
        fullWidth
        variant="contained"
        sx={{ mt: 3, mb: 2 }}
      >
        Sign in
      </Button>
    </Box>
  )
}

export default function Login() {
  return (
    <Layout>
      <Box className={styles.loginForm}>
        <LoginForm />
      </Box>
    </Layout>
  )
}