import { NextApiRequest, NextApiResponse } from "next"
import { withSessionRoute } from "../../lib/session"

export default withSessionRoute(logoutRoute)

function logoutRoute(req: NextApiRequest, res: NextApiResponse) {
  // Remove the iron session cookie
  req.session.destroy()
  res.json({ data: { isLoggedIn: false, username: '' } })
}