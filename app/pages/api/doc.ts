import { withSwagger } from 'next-swagger-doc';

const swaggerHandler = withSwagger({
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'MyWeather Swagger Doc',
      version: '0.1.0',
    },
  },
  schemaFolders: ['models'],
});

export default swaggerHandler();