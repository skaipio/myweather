import { NextApiRequest, NextApiResponse } from 'next'
import { withSessionRoute } from '../../lib/session'
import dbConnect from '../../lib/dbConnect'
import User from '../../models/User'
import crypto from 'crypto'
import IUser from '../../types/user'

export default withSessionRoute(loginRoute)

const invalidCredentialsResponse = {
  success: false,
  message: 'Invalid credentials'
}

// Compare the password of an already fetched user and compare the
// password for a potential match
function validatePassword(user: IUser, inputPassword: string) {
  const inputHash = crypto
    .pbkdf2Sync(inputPassword, user.passwordSalt!, 1000, 64, 'sha512')
    .toString('hex')
  const passwordsMatch = user.passwordHash === inputHash
  return passwordsMatch
}

async function loginRoute(req: NextApiRequest, res: NextApiResponse) {
  // TODO: Check request method and return 404 if invalid
  const { username, password } = await req.body

  await dbConnect()

  const user = await User.findOne({ username })

  if (!user) {
    res.status(401).json(invalidCredentialsResponse)
    return
  }

  if (validatePassword(user, password)) {
    req.session.user = user
    await req.session.save()
    res.status(200).json({
      data: user,
      success: true
    })
  } else {
    res.status(401).json(invalidCredentialsResponse)
  }
}