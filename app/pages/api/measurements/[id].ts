/** A swagger description for the route
 * @swagger
 * /api/measurements/{id}:
 *   put:
 *     description: Updates and returns the measurement with the matching {id}.
 *     parameters:
 *       - name: id
 *         in: path
 *         description: id of the measurement being updated
 *         required: true
 *     responses:
 *       200:
 *         description: The updated measurement.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                 data:
 *                   type: object
 *                   $ref: '#/components/schemas/Measurement'
 *       401:
 *         description: User is not logged in.
 *   delete:
 *     description: Deletes the measurement with the matching {id}.
 *     parameters:
 *       - name: id
 *         in: path
 *         description: id of the measurement being deleted
 *         required: true
 *     responses:
 *       200:
 *         description: The deleted measurement.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                 data:
 *                   type: object
 *                   $ref: '#/components/schemas/Measurement'
 *       400:
 *         description: Bad request.
 *       401:
 *         description: User is not logged in.
 */

import type { NextApiRequest, NextApiResponse } from 'next'
import { sendMessageOnError } from '../../../lib/apiUtil'
import dbConnect from '../../../lib/dbConnect'
import { withSessionRoute } from '../../../lib/session'
import Measurement from '../../../models/Measurement'

const deleteMeasurement = async (req: NextApiRequest, res: NextApiResponse) => {
  const { query: { id } } = req
  await sendMessageOnError(async () => {
    const deletedMeasurement = await Measurement.findByIdAndDelete(id)
    res.status(200).json({ success: true, data: deletedMeasurement })
  }, res)
}

const updateMeasurement = async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, query: { id } } = req
  await sendMessageOnError(async () => {
    const updatedMeasurement = await Measurement.findByIdAndUpdate(id, body)
    res.status(200).json({ success: true, data: updatedMeasurement })
  }, res)
}

export default withSessionRoute(measurementRoute)

async function measurementRoute(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { method, session: { user } } = req

  if (!user) {
    res.status(401).end()
    return
  }

  await dbConnect()

  switch (method) {
    case 'DELETE':
      await deleteMeasurement(req, res)
      break
    case 'PUT':
      await updateMeasurement(req, res)
      break
    default:
      res.status(400)
      break
  }

}
