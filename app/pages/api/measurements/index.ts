/** A swagger description for the route
 * @swagger
 * /api/measurements:
 *   get:
 *     description: Returns all weather measurements provided that the user is logged in.
 *     responses:
 *       200:
 *         description: A list of measurements.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   success:
 *                     type: boolean
 *                   data:
 *                     type: object
 *                     $ref: '#/components/schemas/Measurement'
 *       401:
 *         description: User is not logged in.
 *   post:
 *     description: Create a new measurement.
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             $ref: '#/components/schemas/Measurement'
 *     responses:
 *       201:
 *         description: A new measurement was created and is returned.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                 data:
 *                   type: object
 *                   $ref: '#/components/schemas/Measurement'
 *       400:
 *         description: Bad request.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                 message:
 *                   type: string
 *       401:
 *         description: User is not logged in.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                 message:
 *                   type: string
 */

import type { NextApiRequest, NextApiResponse } from 'next'
import { sendMessageOnError } from '../../../lib/apiUtil'
import dbConnect from '../../../lib/dbConnect'
import { withSessionRoute } from '../../../lib/session'
import Measurement from '../../../models/Measurement'

const getMeasurements = async (req: NextApiRequest, res: NextApiResponse) => {
  await sendMessageOnError(async () => {
    Measurement.ensureIndexes()
    const measurements = await Measurement.find({}).sort({ timestamp: 'desc' }).exec()
    res.status(200).json({ success: true, data: measurements })
  }, res)
}

const createMeasurement = async (req: NextApiRequest, res: NextApiResponse) => {
  const { body } = req
  await sendMessageOnError(async () => {
    const savedMeasurement = await Measurement.create(body)
    res.status(201).json({ success: true, data: savedMeasurement })
  }, res)
}

export default withSessionRoute(measurementsRoute)

async function measurementsRoute(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { body, method, session } = req
  const { user } = session

  if (!user) {
    res.status(401).end()
    return
  }

  await dbConnect()

  switch (method) {
    case 'GET':
      await getMeasurements(req, res)
      break
    case 'POST':
      await createMeasurement(req, res)
      break
    default:
      res.status(400)
      break
  }

}
