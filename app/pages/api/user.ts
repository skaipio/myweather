import { NextApiRequest, NextApiResponse } from 'next'
import { withSessionRoute } from '../../lib/session'

export default withSessionRoute(userRoute)

async function userRoute(req: NextApiRequest, res: NextApiResponse) {
  if (req.session.user) {
    /* Read the username from the session and fetch from db if
      further user details are needed */

    res.json({
      data: {
        ...req.session.user,
        isLoggedIn: true,
      }
    })
  } else {
    res.json({
      data: {
        isLoggedIn: false,
        username: ''
      }
    })
  }
}