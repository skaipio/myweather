import { Container } from '@mui/material'
import type { NextPage } from 'next'
import React from 'react'
import Layout from '../components/Layout'
import MeasurementDataGrid from '../components/MeasurementDataGrid'
import useUser from '../lib/useUser'

const Home: NextPage = () => {
  const { user } = useUser({ redirectTo: '/login' })

  if (!user || !user.isLoggedIn) return null

  return (
    <Layout>
      <Container maxWidth="md">
        <MeasurementDataGrid />
      </Container>
    </Layout>
  )
}

export default Home
